package pl.lacrima.TheFirm.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PostalCodeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PostalCode {


    String message() default "{PostalCode}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}