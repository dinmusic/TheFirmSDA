package pl.lacrima.TheFirm.security.config;

import org.springframework.context.annotation.Configuration;
import pl.lacrima.TheFirm.database.model.User;
import pl.lacrima.TheFirm.security.service.UserService;

import javax.annotation.PostConstruct;

@Configuration
public class AdminConfig {
    private final UserService userService;

    public AdminConfig(UserService userService) {
        this.userService = userService;
    }



    @PostConstruct
    private void registerAdmin() {

      if (!userService.loginExists("admin")) {
            User admin = new User();
            admin.setLogin("admin");
            admin.setPassword("admin");
            admin.setEmail("admin@admin.pl");
            admin.setCity("adminowo");
            admin.setContractorName("adminek");
            admin.setNip("111-222-33-44");
            admin.setPhone("111-111-111");
            admin.setPostalCode("11-111");
            admin.setStreet("adminowska 6");
            userService.registerNewUser(admin);
       }
    }
}
