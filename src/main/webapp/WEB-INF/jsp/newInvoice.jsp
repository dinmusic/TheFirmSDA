<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Warehouse</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/bootstrap-select.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="dist/js/bootstrap-select.js"></script>

    <style type="text/css">
        input[type="text"] {
            width: 100%;
        }

        /* removing this would make input not to go over cells border, but they would be too short, I want them to fit cells size */
        /*body { background: #f2f2f2 !important; }*/

        .mytable > tbody > tr > td, .mytable > tbody > tr > th, .mytable > tfoot > tr > td, .mytable > tfoot > tr > th, .mytable > thead > tr > td, .mytable > thead > tr > th {
            padding-left: 25px;
            padding-right: 25px;
            /*padding-top: 5px;*/
            /*padding-bottom: 5px;*/
        }

    </style>

</head>
<body>
<center>
    <h5>*New Invoice*</h5>

    <button type="button" onclick="document.location.href='allcontractors';" class="btn btn-primary btn-sm">Contractors</button>
    <button type="button" onclick="document.location.href='createcontractor';" class="btn btn-warning btn-sm">New Contractor</button>
    <button type="button" onclick="document.location.href='allinvoices';" class="btn btn-primary btn-sm">Invoices</button>
    <button type="button" onclick="document.location.href='newinvoice';" class="btn btn-sm btn-outline-primary">New Invoice</button>
    <button type="button" onclick="document.location.href='allproducts';" class="btn btn-primary btn-sm">Products</button>
    <button type="button" onclick="document.location.href='newProduct';" class="btn btn-warning btn-sm">New Product</button>

    <form:form action="addinvoice" method="post">

    <div class="container" width="70%">
        <table class="mytable  table-bordered  ">
            <thead>
            <tr>

                <th>Contractor</th>
                <th>Product</th>
                <th>Amount</th>

            </tr>
            </thead>
            <%--<tbody>--%>
            <tr>

                <td>
                    <select class="selectpicker show-tick form-control"  data-live-search="true" data-width="auto" name="contractor">
                        <c:forEach var="contractor" items="${ALL_CONTRACTORS}" varStatus="status">
                            <option value="${contractor.id}"><c:out value="${contractor.contractorName}" ></c:out> </option>
                        </c:forEach>
                    </select>
                <td align="left"/>

                <select class="selectpicker show-tick form-control"  data-live-search="true" data-width="auto" name="warehouse">
                    <c:forEach var="warehouse" items="${ALL_WAREHOUSE}" varStatus="status">
                        <option form:value="${getWare}"><c:out value="${warehouse.productName}"/></option>
                    </c:forEach>
                </select>
                <td align="left"/>


                <input type="text" class="form-control" id="pwd" placeholder="Enter amount" name="amount">
                    <%--<input type="text" name="email"/>--%>
                <td align="left"/>

            </tr>

            <tr>
                <td align="right" colspan="10" td>
                    <button type="button" onclick="history.back();" class="btn btn-primary btn-sm btn-danger">Cancel!
                    </button>
                    <button type="reset" value="Reset" class="btn btn-primary btn-sm">Reset</button>
                    <button type="submit" value="Save" class="btn btn-primary btn-sm btn-success">Save</button>
                </td>
            </tr>

            </tr>

        </table>
    </form:form>
    </div>


</center>
</body>
</html>