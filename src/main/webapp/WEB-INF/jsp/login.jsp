<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Log in</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
    body {
        padding-top: 240px;
        padding-bottom: 240px;
        background-color: #f5f5f5;
    }
    .enjoy-css {
    /*display: inline-block;*/
    /*-webkit-box-sizing: content-box;*/
    /*-moz-box-sizing: content-box;*/
    /*box-sizing: content-box;*/
    /*padding: 10px 20px;*/
    /*border: 1px solid #b7b7b7;*/
    /*-webkit-border-radius: 3px;*/
    /*border-radius: 3px;*/
    /*font: normal 16px/normal "Times New Roman", Times, serif;*/
    /*color: rgba(0,142,198,1);*/
    /*-o-text-overflow: clip;*/
    /*text-overflow: clip;*/
    /*background: rgba(252,252,252,1);*/
    /*-webkit-box-shadow: 2px 2px 2px 0 rgba(0,0,0,0.2) inset;*/
    /*box-shadow: 2px 2px 2px 0 rgba(0,0,0,0.2) inset;*/
    /*text-shadow: 1px 1px 0 rgba(34,14,247,0.66) ;*/
    /*-webkit-transition: all 200ms cubic-bezier(0.42, 0, 0.58, 1);*/
    /*-moz-transition: all 200ms cubic-bezier(0.42, 0, 0.58, 1);*/
    /*-o-transition: all 200ms cubic-bezier(0.42, 0, 0.58, 1);*/
    /*transition: all 200ms cubic-bezier(0.42, 0, 0.58, 1);*/
    }

</style>

</head>
<body>
<center>
<form:form action="/user/login" method="post" modelAttribute="user" cssClass="form-signin">
    <h2 class="form-signin-heading">Please Log In</h2>

    <form:errors cssClass="alert alert-error" element="div" />
    <form:input path="login" cssClass="enjoy-css"  placeholder="Login" />
    <form:password path="password" cssClass="enjoy-css"  placeholder="Password"  />

    <input type="submit" value="Log In" class="btn btn-large btn-primary" />
</form:form>
</center>
</body>
</html>